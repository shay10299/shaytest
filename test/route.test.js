const supertest = require("supertest");
const assert = require('assert');
const app = require("../app");

describe("GET /", function() {
    it("it should has status code 200", function(done) {
      supertest(app)
        .get("/")
        .expect(200)
        .end(function(err, res){
          if (err) done(err);
          else
            done();
        });
    });
  });
  describe("GET /", function() {
    it("it should send string calculates sqrt", function(done) {
      supertest(app)
        .get("/")
        .expect("calculates sqrt!")
        .end(function(err, res){
          if (err) done(err);
          else   
            done();
        });
    });
  });
  describe("GET /", function() {
    it("it should send sqrt", function(done) {
      supertest(app)
        .get("/sqrt/25")
        .expect("sqrt is 5")
        .end(function(err, res){
          if (err) done(err);
          else   
            done();
        });
    });
  });