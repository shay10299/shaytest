const assert = require('assert');
const math=require("../utils/sqrt")
describe('Math Test', () => {
 it('should return 6', () => {
        assert.equal(math.sqrt1(36), 6);
    });
 it('should return 10', () => {
        assert.equal(math.sqrt1(100), 10);
    });
});