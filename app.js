const express = require('express')
const app = express()
const math = require('./utils/sqrt.js')
const port = process.env.PORT || 3000;

app.get('/', (req, res) => {
  res.send('calculates sqrt!')
})
app.get('/login', (req, res) => {
  res.send('login page!')
})
app.get('/sqrt/:value', (req, res) => {
    res.send("sqrt is "+math.sqrt1(req.params.value))
})
app.listen(port, () => {
  console.log(`sqrt app listening at http://localhost:${port}`)
})

module.exports= app